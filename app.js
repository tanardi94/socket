var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var cors = require('cors');
var http = require('http').Server(app);
var io = require('socket.io')(http, {origins: '*:*'});
var port = process.env.PORT || 4000;
var jwt_decode = require('jwt-decode');
// Start the Server
http.listen(port, function () {
    console.log('Server Started. Listening on :' + port);
});
// Express Middleware
const apiRoutes = require('./api-routes');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());
// Render Main HTML file
app.get('/', function (req, res) {
    res.sendFile('views/map.html', {
        root: __dirname
    });
});

app.get('/publish', function (req, res) {
    res.sendFile('views/publisher.html', {
        root: __dirname
    });
});

app.get("/rooms", (req, res) => {
    res.sendFile(__dirname + "/views/index.html");
  })

app.use('/api', apiRoutes);

// io.on('connection', function (socket) {
//     console.log('socket created');
//     let previousId;
//     const safeJoin = currentId => {
//         socket.leave(previousId);
//         socket.join(currentId);
//         previousId = currentId;
//       };
//     socket.on('disconnect', function() {
//       console.log('Got disconnect!');
//    });
//    socket.on('lastKnownLocations', function (data) {
            
//             client.set('position', data.Coordinate.Latitude + ', ' + data.Coordinate.Longitude);
//             console.log(data);
//      });
//    socket.on('lastKnownLocation', function (data) {
            
//             client.set('position', data.Coordinate.Latitude + ', ' + data.Coordinate.Longitude);
//             console.log(data);
//      });
//   });


var users = []
io.on('connection', function(socket) {
    // console.log(`Connection : Socket_id = ${socket.id}`);

    socket.on('subscribe', data => {
        const userName = data.userName;
        const roomName = data.roomName;

        try {
            var decoded = jwt_decode(userName, { header: true});
            socket.join(roomName);
            // console.log(`Username: ${userName} joins ${roomName} room`);
            socket.emit('newUserToChatRoom', {
                roomName: roomName,
                userName: userName
            });
        } catch (error) {
            socket.emit('authenticateError', 'Otentikasi user error');
        }


    })

    socket.on('unsubscribe', data => {
        // console.log('unsubscribe triggered');
        
        // const userName = data.userName
        const roomName = data.roomName
        const userName = data.userName

        // console.log(`Username: ${userName} has left room: ${roomName}`);
        io.to(`${roomName}`).emit('userLeft', userName)
        socket.leave(`${roomName}`)
    })

    socket.on('newData', data => {
        // console.log('newData triggered')
        // console.log(data)a
        const roomName = data.roomName

        io.to(`${roomName}`).emit('updateData', {
            roomName: roomName,
            message: data.message,
            interval: 2
        })
    })

    socket.on('disconnect', function () {
        console.log("One of sockets disconnected from our server.")
    });
});

